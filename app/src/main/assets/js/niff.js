$(function () {
    $('.sidenav').sidenav();
    $('.fixed-action-btn').floatingActionButton();
    $('.modal').modal();
     //creacion de preguntas y respuestas
     var niff = new Array();
    //nivel1
    niff[0]=[
        ["0","¿Cuál es el nombre del organismo que ha tomado el liderazgo en materia de normas internacionales de Contabilidad?",
            [
                ["0","IASB", "1"],
                ["1","ISAB", "0"],
                ["2","IASBI", "0"],
                ["3","Ninguna de las anteriores", "0"]
            ]
        ],
        ["1","¿Cómo se denominan las normas internacionales de contabilidad?",
            [
                ["0","NIC Y NIF", "0"],
                ["1","NIC Y NIIF", "1"],
                ["2","NIR Y NIS", "0"],
                ["3","NIIF", "0"]
            ]
        ],
        ["2","¿En Colombia cual ley estableció la convergencia de la Contabilidad de las empresas colombianas hacia las Normas Internacionales de Contabilidad NIIFS?",
            [
                ["0","La ley 1414 de julio de 1009", "0"],
                ["1","La ley 1501 de julio de 1009", "0"],
                ["2","La ley 1301 de julio de 1009", "0"],
                ["3","La ley 1314 de julio de 1009", "1"]
            ]
        ],
        ["3","¿Qué entidad estableció los grupos o categorías de empresas para proceder al proceso de convergencia?",
            [
                ["0","La superintendencia de sociedades", "1"],
                ["1","La superintendencia de contadores", "0"],
                ["2","La DIAN", "0"],
                ["3","Todas las anteriores", "0"]
            ]
        ],
        ["4","¿Qué es contabilidad para las NIIF?",
            [
                ["0","Sistema de control monetario de las empresas", "0"],
                ["1","Sistemas de control económico, financiero y social", "1"],
                ["2","Sistema de control logístico", "0"],
                ["3","Ninguna de las anteriores", "0"]
            ]
        ]
    ];
    //nivel2
    niff[1]=[
        ["0","Nombre 3 de las Excepciones Obligatorias",
            [
                ["0","Estimaciones, Contabilidad de Coberturas, Derivados Implícitos.", "1"],
                ["1","Activos, Pasivos, Gastos.", "0"],
                ["2","La medición, La estimación, La transición", "0"],
                ["3","los flujos Efectivo, los flujos del activo, los flujos financieros", "0"]
            ]
        ],
        ["1","¿Cuáles son Exenciones?",
            [
                ["0","Pagos Basados en Acciones, a los instrumentos de patrimonio, pasivos surgidos de transacciones con pagos basados en acciones liquidados", "0"],
                ["1","Al valor razonable, Al costo, Propiedades de inversión", "0"],
                ["2","Combinación de Negocios, Contratos de seguro, Costo atribuido", "1"],
                ["3","Los criterios de reconocimiento, Activos intangibles, un mercado activo", "0"]
            ]
        ],
        ["2","Activos intangibles",
            [
                ["0","Licencias", "0"],
                ["1","Derechos", "0"],
                ["2","Marcas", "0"],
                ["3","Todas las anteriores", "1"]
            ]
        ],
        ["3","¿Plusvalía ó Goodwill en adquisición de compañías?",
            [
                ["0","Modelo de costo-deterioro", "0"],
                ["1","Vida útil indefinida", "0"],
                ["2","Prueba anual de deterioro", "0"],
                ["3","Todas las anteriores", "1"]
            ]
        ],
        ["4","¿Cuáles son las características de la Propiedad, planta y equipo?",
            [
                ["0","Revisión anual obligatoria del valor residual", "0"],
                ["1","la vida útil", "0"],
                ["2","El método de depreciación", "0"],
                ["3","Todas las anteriores", "1"]
            ]
        ]
    ];
    //nivel3
    niff[2]=[
        ["0","¿Por qué se deben unificar las normas y criterios contables en los diferentes países del mundo?",
            [
                ["0","Que posibilita una ampliación de mercados", "0"],
                ["1","Incrementa la negociación en los mercados internacionales, ofreciendo una información homogénea", "1"],
                ["2","Ofrece una mejor calidad del servicio en las actividades diarias de la operación", "0"],
                ["3","Fortalece la buena comunicación entre los socios", "0"]
            ]
        ],
        ["1","¿Cuál es el nombre del organismo que ha tomado el liderazgo en materia de normas internacionales de Contabilidad y en dónde está su sede?",
            [
                ["0","Agencia para el manejo de los estándares locales de contabilidad. Sede reino unido", "0"],
                ["1","Agencia para el control de estándares internacionales de contabilidad. Sede Francia", "0"],
                ["2","Agencia para el manejo de los estándares internacionales de contabilidad. Sede Londres", "1"],
                ["3","Agencia para el reconocimiento y manejo de estándares internacionales de contabilidad Londres", "0"]
            ]
        ],
        ["2","Cómo se denominan las normas internacionales de contabilidad hoy en día, ¿Cuál es la sigla y qué significa?",
            [
                ["0","NIIC norma internacional de información financiera Hacen referencia a los estados financieros", "0"],
                ["1","NIFC norma de información financiera contable financiera Hacen referencia a los estados financieros", "0"],
                ["2","NIIF norma internacional de información física Hacen referencia a los estados financieros", "0"],
                ["3","NIIF Normas Internacionales de Información financiera Hacen referencia a los estados financieros", "1"]
            ]
        ],
        ["3","Cuál es el objetivo de las NIIF, ¿qué información se considera de alta calidad?",
            [
                ["0","sea transparente para los usuarios y comparable", "1"],
                ["1","que tenga un alcance eficaz en la toma de decisiones", "0"],
                ["2","que sea privada", "0"],
                ["3","que conserve su validez", "0"]
            ]
        ],
        ["4","¿Cuáles son los requisitos para el reconocimiento y medición para quienes adoptan las NIF por primera vez?",
            [
                ["0","Con aplicación de Exenciones", "0"],
                ["1","Con aplicación de Excepciones", "0"],
                ["2","Con aplicación retroactiva", "1"],
                ["3","Con aplicación general", "0"]
            ]
        ]
    ];
    // Se guarda en localStorage despues de JSON stringificarlo 
    localStorage.setItem('Arrayniff', JSON.stringify(niff));    
});

