$(function () {
    $('.sidenav').sidenav();
    $('.fixed-action-btn').floatingActionButton();
    $('.modal').modal();

    $('.nivel').click(function () {
        // Guardar Nivel seleccionado
        localStorage.setItem("level", $(this).data('level'));
        window.location='../pages/juego.html'; 
    });

});