$(function () {
    $('.sidenav').sidenav();
    $('.fixed-action-btn').floatingActionButton();
    $('.modal').modal();
    $('.modal').modal('open');
    $('.scrollspy').scrollSpy();
    //obtener nivel seleccionado
    var level = localStorage.getItem("level");
    // Obtener el arreglo de preguntas localStorage
    var niff = localStorage.getItem('Arrayniff');
    // Se parsea para poder ser usado en js con JSON.parse :)
    arrayniff = JSON.parse(niff);
    iniciojuego(level - 1);

    //iniciamos en localstorage el puntaje 
    localStorage.setItem("puntaje", 0);

    function iniciojuego(nivel) {
        arrayazar = azar(4);
        var i = 0;
        lanzarpregunta(nivel, arrayazar[i]);
        $('#responder').click(function () {
            if ($("input:checked").val()) {
                i = i + 1;
                valrta = validate($("input:checked").val());
                if (valrta === true) {
                    if (i < 5) {
                        lanzarpregunta(nivel, arrayazar[i]);
                    } else {
                        $('#responder').hide();
                        finjuego();
                    }
                }
            } else {
                swal({
                    title: "¡Importante!",
                    text: "Recuerde que debe debe seleccionar la respuesta que considera correcta",
                    icon: "error",
                    button: "Volver"
                });
            }
        });
    }

    function lanzarpregunta(nivel, id) {
        $('input:checked').prop('checked', false);
        var azarpr = azar(3);
        $('#pregunta').html(arrayniff[nivel][id][1]);
        $('#val1').val(arrayniff[nivel][id][2][azarpr[0]][2]);
        $('#rta1').html(arrayniff[nivel][id][2][azarpr[0]][1]);
        $('#val2').val(arrayniff[nivel][id][2][azarpr[1]][2]);
        $('#rta2').html(arrayniff[nivel][id][2][azarpr[1]][1]);
        $('#val3').val(arrayniff[nivel][id][2][azarpr[2]][2]);
        $('#rta3').html(arrayniff[nivel][id][2][azarpr[2]][1]);
        $('#val4').val(arrayniff[nivel][id][2][azarpr[3]][2]);
        $('#rta4').html(arrayniff[nivel][id][2][azarpr[3]][1]);
    }

    function azar(max) {
        var arrayazar = [];
        for (var i = 0; i <= max; i++) {
            var aux = Math.round(Math.random() * max);
            var aux2 = 0;
            while (aux2 < i) {
                if (aux != arrayazar[aux2])
                    aux2++;
                else {
                    aux = Math.round(Math.random() * max);
                    aux2 = 0;
                }
            }
            arrayazar[i] = aux;
        }
        return arrayazar;
    }
    function validate(value) {
        var puntaje = localStorage.getItem("puntaje");
        if (value == 1) {
            puntaje = parseInt(puntaje) + 1;
            localStorage.setItem("puntaje", puntaje);
            swal({
                title: "¡Correcto!",
                text: "",
                icon: "success",
                button: "siguiente"
            });
            return true;
        } else {
            swal({
                title: "¡Tu respuesta es errada!",
                text: "",
                icon: "error",
                button: "siguiente"
            });
            return true;
        }
    }
    function nombrelevel(nivel) {
        if(nivel==1){
            return "principiante";
        }else if(nivel==2){
            return "intermedio";
        }else if(nivel==3){
            return "avanzado";
        }
    }
    var nombrenivel =nombrelevel(level);
    function finjuego(){
        $('#pregunta').html("Tu puntaje final en el nivel "+nombrenivel+"  fue: ");
        $('#respuestas').html('<hr><p class="flow-text" style="font-size: 150%;"> <b>'+localStorage.getItem("puntaje")+'</b> Punto(s) de 5 posibles <br></p><hr><br><br><p class="flow-text">Presiona Salir para continuar...</p>');
    }
});